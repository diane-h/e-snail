CREATE TABLE snails (
    id TEXT,
    title TEXT,
    content TEXT,
    publish_date INTEGER,
    author TEXT
);

CREATE TABLE users (
    username TEXT NOT NULL,
    passhash TEXT NOT NULL,
    roles TEXT
);

INSERT INTO
    users (username, passhash, roles)
VALUES
    (?, ?, 'admin,user,manager');

CREATE TABLE todos (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    task TEXT NOT NULL,
    done BOOLEAN NOT NULL DEFAULT false
);