use axum::extract::DefaultBodyLimit;
use axum::middleware;
use axum::routing::{get, post};
use axum::{extract::Request, http::StatusCode, middleware::Next, response::IntoResponse, Router};
use dashmap_cache::DashmapCache;
use reqwest;
use reqwest::redirect::Policy;
use std::net::SocketAddr;
use std::sync::Arc;
use std::time;
use stores::database::Repository;
use tower_sessions::cookie::time::Duration;
use tower_sessions::Expiry;
use tower_sessions::MemoryStore;
use tower_sessions::SessionManagerLayer;
// pub mod config
pub mod rest;
pub mod schemas;
pub mod services;
pub mod stores;

#[derive(Clone)]
pub struct Repositories {
    pub rqwc: reqwest::Client,
    pub cache: Arc<DashmapCache>,
    pub db: Repository,
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt().json().init();
    /*
    let config = Config::from_env();
    */
    let repos = Repositories {
        db: Repository::new().await,
        cache: Arc::new(DashmapCache::new()),
        rqwc: reqwest::Client::builder()
            .redirect(Policy::none())
            .build()
            .unwrap(),
    };

    let session_store = MemoryStore::default();
    let session_layer = SessionManagerLayer::new(session_store)
        .with_expiry(Expiry::OnInactivity(Duration::minutes(30)));

    let app = Router::new()
        .route("/login", post(rest::login))
        .route("/logout", post(rest::logout))
        .route("/post", get(rest::browse))
        .route("/post/:author/:id", get(rest::get_snail))
        .route("/post", post(rest::post_snail))
        .layer(session_layer)
        .layer(middleware::from_fn(log_access))
        .layer(DefaultBodyLimit::max(50 * 1024))
        .with_state(repos);

    let addr = SocketAddr::from(([127, 0, 0, 1], 8061));
    tracing::debug!("listening on {addr}");
    let listener = tokio::net::TcpListener::bind(addr).await.unwrap();
    axum::serve(listener, app).await.unwrap();
}

async fn log_access(req: Request, next: Next) -> Result<impl IntoResponse, (StatusCode, String)> {
    let t0 = time::Instant::now();
    let uri = req.uri().to_owned().to_string();

    let res = next.run(req).await;

    let t = (time::Instant::now() - t0).as_millis();
    tracing::info!(uri = uri, time_ms = t);
    Ok(res)
}
