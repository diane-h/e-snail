use std::time::SystemTime;

use crate::{schemas::SnailForm, stores::database::Repository};

pub enum ServiceError {
    DbError(sqlx::error::Error),
    NotFound,
    Unauthorized,
}

impl From<sqlx::error::Error> for ServiceError {
    fn from(value: sqlx::error::Error) -> Self {
        return Self::DbError(value);
    }
}

impl ToString for ServiceError {
    fn to_string(&self) -> String {
        match self {
            Self::DbError(err) => err.to_string(),
            Self::NotFound => "Not found".to_string(),
            Self::Unauthorized => "Unauthorized".to_string(),
        }
    }
}

pub async fn get_snail_text(
    repo: Repository,
    user_id: String,
    author: String,
    snail_id: String,
) -> Result<String, ServiceError> {
    let snail = repo
        .get_snail_by_id(author.as_str(), snail_id.as_str())
        .await?;
    if snail.author != user_id
        && snail.publish_date
            < SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs() as i64
                - 86400
    {
        return Err(ServiceError::NotFound);
    }
    Ok(snail.content)
}

pub async fn post_snail_text(
    repo: Repository,
    user_id: String,
    mut snail_form: SnailForm,
) -> Result<(), ServiceError> {
    let cur_author_pending = repo
        .get_pending_snail_count_by_author(user_id.as_str(), 86400)
        .await?;
    if cur_author_pending >= 3 {
        return Err(ServiceError::Unauthorized);
    }
    snail_form.content = markdown::to_html(snail_form.content.as_str());
    let id = slug::slugify(&snail_form.title);
    Ok(repo
        .save_snail(user_id.as_str(), id.as_str(), snail_form)
        .await?)
}
