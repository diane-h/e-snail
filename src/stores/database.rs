use std::time::SystemTime;

use argon2::{password_hash::SaltString, Argon2, PasswordHasher, PasswordVerifier};
use serde::{Deserialize, Serialize};
use sqlx::prelude::FromRow;
use sqlx::sqlite::{SqliteConnectOptions, SqlitePool};
use sqlx::{Pool, Row, Sqlite};

use crate::schemas::SnailForm;

#[derive(Debug, FromRow, Clone, Serialize, Deserialize)]
pub struct Snail {
    pub content: String,
    pub publish_date: i64,
    pub author: String,
}

#[derive(Debug, FromRow, Clone)]

pub struct UserData {
    pub roles: String,
    pub passhash: String,
}

#[derive(Debug, Clone)]
pub struct Repository {
    pool: Pool<Sqlite>,
}

impl Repository {
    pub async fn new() -> Self {
        Self {
            pool: SqlitePool::connect_with(
                SqliteConnectOptions::new()
                    .filename("example.db")
                    .create_if_missing(true),
            )
            .await
            .unwrap(),
        }
    }
    pub async fn get_snail_by_id(
        &self,
        author: &str,
        snail_id: &str,
    ) -> Result<Snail, sqlx::error::Error> {
        sqlx::query_as::<_, Snail>(
            r#"
        SELECT * FROM snails WHERE id = ? AND author = ? 
        "#,
        )
        .bind(snail_id)
        .bind(author)
        .fetch_one(&self.pool)
        .await
    }

    pub async fn get_pending_snail_count_by_author(
        &self,
        username: &str,
        offset: i64,
    ) -> Result<i64, sqlx::error::Error> {
        Ok(sqlx::query(
            r#"
        SELECT COUNT(*) FROM snails WHERE author = ? AND publish_date < ?
        "#,
        )
        .bind(username)
        .bind(
            SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs() as i64
                - offset,
        )
        .fetch_one(&self.pool)
        .await?
        .get::<i64, _>(0))
    }

    pub async fn save_snail(
        &self,
        username: &str,
        id: &str,
        snail: SnailForm,
    ) -> Result<(), sqlx::error::Error> {
        let _result = sqlx::query(
            r#"
                INSERT INTO snails (id, author, title, content, publish_date) VALUES (?, ?, ?, ?, ?);
                "#,
        )
        .bind(id)
        .bind(username)
        .bind(snail.title)
        .bind(snail.content)
        .bind(
            SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs() as i64,
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }

    pub async fn check_user(&self, username: &str, password: &str) -> Result<Vec<String>, String> {
        let result = sqlx::query_as::<_, UserData>(
            r#"
            SELECT roles, passhash FROM users WHERE username = ?
            "#,
        )
        .bind(username)
        .fetch_one(&self.pool)
        .await;
        match result {
            Err(error) => return Err(error.to_string()),
            Ok(user_data) => {
                tracing::info!(user_data.passhash);
                let parsed_hash = argon2::PasswordHash::new(&user_data.passhash).unwrap();
                match Argon2::default().verify_password(password.as_bytes(), &parsed_hash) {
                    Ok(_) => Ok(user_data
                        .roles
                        .split(",")
                        .map(|s| s.to_string())
                        .collect::<Vec<String>>()),
                    _ => Err("Authentication failed".to_owned()),
                }
            }
        }
    }

    pub async fn register_user(&self, username: String, password: String) -> Result<(), String> {
        let salt = SaltString::generate(&mut argon2::password_hash::rand_core::OsRng);

        let argon2 = Argon2::default();

        let password_hash = argon2
            .hash_password(&password.as_bytes(), &salt)
            .unwrap()
            .to_string();

        let result = sqlx::query(
            r#"
                INSERT INTO users (username, passhash, roles) VALUES (?, ?, "user");
                "#,
        )
        .bind(username)
        .bind(password_hash)
        .execute(&self.pool)
        .await;

        match result {
            Ok(_) => Ok(()),
            Err(error) => Err(error.to_string()),
        }
    }
}
