use crate::schemas::{LoginForm, SnailForm};
use crate::{services, Repositories};
use axum::extract::{FromRequestParts, Path, State};
use axum::http::request::Parts;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Redirect};
use axum::{async_trait, Form};
use tower_sessions::Session;

pub async fn login(
    State(repo): State<Repositories>,
    session: Session,
    Form(fdata): Form<LoginForm>,
) -> impl IntoResponse {
    match repo
        .db
        .check_user(fdata.username.as_str(), fdata.password.as_str())
        .await
    {
        Ok(roles) => {
            session.insert("roles", &roles).await.unwrap();
            session
                .insert("user", fdata.username.as_str())
                .await
                .unwrap();
            return Redirect::to("/browse").into_response();
        }
        Err(error) => return error.into_response(),
    }
}

pub struct AuthenticatedUser {
    pub user_id: String,
}

#[async_trait]
impl<S> FromRequestParts<S> for AuthenticatedUser
where
    S: Send + Sync,
{
    type Rejection = (StatusCode, &'static str);

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
        let session = parts.extensions.get::<Session>().cloned().ok_or((
            StatusCode::INTERNAL_SERVER_ERROR,
            "Can't extract session. Is `SessionManagerLayer` enabled?",
        ))?;
        let user_id = session
            .get::<String>("user")
            .await
            .unwrap_or(None)
            .ok_or((StatusCode::UNAUTHORIZED, "Unauthorized"))?;
        Ok(Self { user_id })
    }
}

pub async fn logout(session: Session) -> impl IntoResponse {
    session.clear().await;
    Redirect::to("/login").into_response()
}

pub async fn browse() -> impl IntoResponse {}

pub async fn get_snail(
    State(repo): State<Repositories>,
    user: AuthenticatedUser,
    Path((author, id)): Path<(String, String)>,
) -> impl IntoResponse {
    match services::get_snail_text(repo.db, user.user_id, author, id).await {
        Ok(snail_content) => (StatusCode::OK, snail_content),
        Err(services::ServiceError::NotFound) => (StatusCode::NOT_FOUND, "Not found".to_owned()),
        Err(services::ServiceError::DbError(err)) => {
            tracing::info!("{}", err.to_string());
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                "Internal server error".to_owned(),
            )
        }
        Err(services::ServiceError::Unauthorized) => {
            (StatusCode::NOT_FOUND, "Not found".to_owned())
        }
    }
}

pub async fn post_snail(
    State(repo): State<Repositories>,
    user: AuthenticatedUser,
    Form(fdata): Form<SnailForm>,
) -> impl IntoResponse {
    match services::post_snail_text(repo.db, user.user_id, fdata).await {
        Ok(_) => StatusCode::CREATED,
        Err(err) => {
            tracing::info!("{}", err.to_string());
            StatusCode::BAD_REQUEST
        }
    }
}
